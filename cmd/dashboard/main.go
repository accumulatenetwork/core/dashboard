package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/core/dashboard/pkg/dashboard"
)

func main() {
	_ = cmd.Execute()
}

var cmd = &cobra.Command{
	Use:  "dashboard",
	Args: cobra.NoArgs,
	Run:  run,
}

var flag = struct {
	Local  string
	Listen string
}{}

func init() {
	cmd.Flags().StringVarP(&flag.Listen, "listen", "l", ":8443", "Address to listen on")
	cmd.Flags().StringVar(&flag.Local, "local", "", "Use local files instead of embedded ones")
}

func run(*cobra.Command, []string) {
	max := big.NewInt(1e9)
	max.Mul(max, max)
	serial, err := rand.Int(rand.Reader, max)
	check(err)

	ca := &x509.Certificate{
		SerialNumber: serial,
		Subject: pkix.Name{
			Organization:  []string{"DeFi Devs"},
			Country:       []string{"US"},
			Province:      []string{""},
			Locality:      []string{""},
			StreetAddress: []string{""},
			PostalCode:    []string{""},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	check(err)

	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	check(err)

	caPEM := new(bytes.Buffer)
	check(pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	}))

	caPrivKeyPEM := new(bytes.Buffer)
	check(pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(caPrivKey),
	}))

	cert, err := tls.X509KeyPair(caPEM.Bytes(), caPrivKeyPEM.Bytes())
	check(err)

	ln, err := net.Listen("tcp", flag.Listen)
	check(err)
	defer ln.Close()
	fmt.Printf("Listening on %v\n", ln.Addr())

	srv := new(http.Server)
	srv.Handler = dashboard.NewHandler(flag.Local)
	srv.TLSConfig = new(tls.Config)
	srv.TLSConfig.Certificates = append(srv.TLSConfig.Certificates, cert)
	check(srv.ServeTLS(ln, "", ""))
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%v", err)
	}
}
