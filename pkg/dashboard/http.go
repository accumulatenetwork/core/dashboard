package dashboard

import (
	"embed"
	_ "embed"
	"io/fs"
	"net/http"
)

//go:embed static/*
var static embed.FS

func NewHandler(local string) http.Handler {
	mux := http.NewServeMux()
	mux.Handle("/proxy", &JsonRpcProxy{hosts: map[string]bool{
		"accumulatenetwork.io": true,
		"detroitledger.tech":   true,
		"dynamicmining.cloud":  true,
		"federate-this.com":    true,
		"sphereon.com":         true,
		"38.135.195.84":        true,
		"195.191.219.115":      true,
		"20.79.250.168":        true,
		"15.223.88.158":        true,
		"34.123.194.109":       true,
		"52.1.44.221":          true,
		"18.218.131.215":       true,
		"95.217.142.50":        true,
		"50.17.246.3":          true,
		"65.109.48.173":        true,
		"104.248.171.142":      true,
	}})

	if local == "" {
		s, err := fs.Sub(static, "static")
		if err != nil {
			panic(err)
		}
		mux.Handle("/", http.FileServer(http.FS(s)))
	} else {
		mux.Handle("/", http.FileServer(http.Dir(local)))
	}
	return mux
}
