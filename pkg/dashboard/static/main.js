const svcTendermintP2P = 0
const svcTendermintRpc = 1
const svcWebsite = 2
const svcPrometheus = 3
const svcAccumulateApi = 4

const { enc: { Hex, Base64 } } = CryptoJS

function parsePartition(chainId) {
    chainId = chainId.toLowerCase()
    const i = chainId.lastIndexOf('-')
    if (i >= 0)
        chainId = chainId.substring(i+1)
    return chainId
}

const Ascending = +1
const Descending = -1

function getter(col) {
    switch (true) {
        case col === 'name':         return x => x.operator() || x.addressUrl().hostname || ''
        case col === 'version':      return x => x.version()
        case col === 'partition':    return x => x.validator().partition
        case col === 'validatorId':  return x => x.validator().id
        case col === 'validatorKey': return x => x.validator().key
        case col === 'dnId':         return x => x.dn().id
        case col === 'dnVoting':     return x => x.dn().voting_power
        case col === 'bvnId':        return x => x.bvn().id
        case col === 'bvnVoting':    return x => x.bvn().voting_power
    }
}

function sorter(col, dir) {
    return {
        col,
        dir: ko.observable(dir),
        get: getter(col),
        reverse() {
            this.dir(this.dir() == Ascending ? Descending : Ascending)
        }
    }
}

ko.bindingHandlers['sort-by'] = {
    init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        element.style.cursor = 'pointer'

        const { sortBy } = viewModel
        element.addEventListener('click', e => {
            element.classList.remove('sort-asc', 'sort-desc')
            const col = valueAccessor()
            const i = sortBy().findIndex(s => s.col === col)
            if (i >= 0) {
                if (e.ctrlKey || e.metaKey) {
                    sortBy.splice(i, 1)
                } else {
                    const s = sortBy()[i]
                    s.reverse()
                    element.classList.add(`sort-${s.dir() === Ascending ? 'asc' : 'desc'}`)
                }
                return
            }

            element.classList.add('sort-asc')
            s = sorter(col, Ascending)
            if (e.ctrlKey || e.metaKey) {
                sortBy.push(s)
            } else {
                sortBy([s])
                for (const tr of element.parentElement.parentElement.children)
                    for (const th of tr.children)
                        if (th !== element)
                            th.classList.remove('sort-asc', 'sort-desc')
            }
        })
    }
}

class ViewModel {
    constructor(bootstrapPeers) {
        this.bootstrapPeers = bootstrapPeers.map(x => new URL(x))
        this.peers = ko.observableArray(bootstrapPeers.map(x => new Peer(this, x)))
        this.readyToStart = ko.computed(() => this.peers().every(x => !!x.addressUrl()))
        this.started = ko.observable(false)
        this.validators = ko.observableArray([])
        this.sortBy = ko.observableArray([
            sorter('partition', Ascending),
            sorter('validatorId', Ascending),
        ])

        this.maxHeight = ko.computed(() => {
            const maxHeight = {}
            for (const peer of this.peers()) {
                if (!peer.dnStatus() || !peer.bvnStatus()) continue

                const { height: dnHeight } = peer.dn() || {}
                if (!dnHeight) continue
                if (!('directory' in maxHeight) || dnHeight > maxHeight.directory)
                    maxHeight.directory = dnHeight

                const { partition } = peer.validator()
                const { height: bvnHeight } = peer.bvn()
                if (!(partition in maxHeight) || bvnHeight > maxHeight[partition])
                    maxHeight[partition] = bvnHeight
            }
            return maxHeight
        })
        this.missingValidators = ko.computed(() => {
            const have = {}
            for (const peer of this.peers())
                if (peer.dnStatus())
                    have[peer.validator().id] = true
            const missing = []
            for (const val of this.validators())
                if (!have[val.publicKeyHash.substr(0,40)])
                    missing.push(new UnknownValidator(val))
            return missing.sort((a, b) => a.publicKeyHash.localeCompare(b.publicKeyHash))
        })

        this.filterActive = ko.observable(false)
        this.showValKey = ko.observable(false)
    }

    addBlankPeer() {
        this.peers.push(new Peer(this))
    }

    removePeer(index) {
        this.peers.splice(index, 1)
    }

    async addPeerUrl(url) {
        const peer = new Peer(this, url)
        if (!peer.addressUrl().hostname) {
            console.warn('Invalid peer', url)
            return
        }

        // Already have this peer? Check by URL.
        if (this.peers().some(x => x.matchesUrl(peer.addressUrl())))
            return

        // Load details
        await peer.refresh(this).catch(log)

        // Don't add peers that don't respond
        if (!peer.accVersion())
            return

        // Already have this peer? Check by validator ID.
        if (this.peers().some(x => x.validator().id == peer.validator().id))
            return

        this.peers.push(peer)
    }

    start() {
        this.started(true)
        this.updateValidators().catch(log)
        for (const peer of this.peers()) {
            peer.refresh(this).catch(log)
        }

        setInterval(() => {
            this.updateValidators().catch(log)
            for (const peer of this.peers()) {
                peer.refreshStatus().catch(log)
            }
        }, 5000)

        setInterval(() => {
            this.updateValidators().catch(log)
            for (const peer of this.peers()) {
                peer.refreshInfo().catch(log)
            }
        }, 30000)
    }

    async updateValidators() {
        const url = this.bootstrapPeers[0]
        const resp = await fetch('/proxy', {
            method: 'post',
            body: JSON.stringify({
                jsonrpc: '2.0',
                id: 1,
                method: 'describe',
                params: {
                    host: url.hostname,
                    basePort: Number(url.port),
                    directory: true,
                    service: svcAccumulateApi,
                    params: {},
                }
            })
        })
        if (!resp.ok) {
            const body = await resp.text()
            console.warn(`Request failed with ${resp.status} ${resp.statusText}: ${body}`)
            return null
        }
        const { result: { values: { network } } } = await resp.json()
        this.validators(network.validators)
    }

    comparePeers(a, b) {
        for (const { dir, get } of this.sortBy()) {
            const va = get(a)
            const vb = get(b)
            switch (true) {
                case va == vb: continue
                case va == '': return +1
                case vb == '': return -1
                case va < vb : return -dir()
                default      : return +dir()
            }
        }
        return 0
    }
}

class UnknownValidator {
    constructor({
        publicKey = '',
        publicKeyHash = '',
        operator = '',
        partitions = [],
        ...other
    }) {
        if (partitions.some(x => x.id.toLowerCase().indexOf('bvn') >= 0)) {
            console.log('wtf')
        }
        Object.assign(this, { publicKey, publicKeyHash, operator, partitions, ...other })
    }

    copyValidatorKey() {
        navigator.clipboard.writeText(this.publicKey).catch(log)
    }
}

class Peer {
    constructor(vm, value = null) {
        this.vm = vm
        this.address = ko.observable(value)
        this.addressUrl = ko.computed(() => {
            try {
                return new URL(this.address())
            } catch (error) {
                return null
            }
        })
        this.accVersion = ko.observable(null)
        this.dnStatus = ko.observable(null)
        this.bvnStatus = ko.observable(null)
        this.dnPeers = ko.observable(null)
        this.bvnPeers = ko.observable(null)

        this.nodeLink = ko.computed(() => `http://${this.addressUrl().hostname}:${Number(this.addressUrl().port)+svcTendermintRpc}`)

        this.dnPeers.subscribe(x => {
            for (let { remote_ip, node_info: { id, listen_addr } } of (x || [])) {
                const i = listen_addr.indexOf('://')
                listen_addr = 'http://' + (i < 0 ? listen_addr : listen_addr.substring(i+3))
                const u = new URL(listen_addr)
                this.vm.addPeerUrl(`http://${id}@${remote_ip}:${u.port}`)
            }
        })

        this.version = ko.computed(() => this.accVersion()?.version)
        this.validator = ko.computed(() => ({
            partition: parsePartition(this.bvnStatus()?.node_info.network || ''),
            id: this.dnStatus()?.validator_info.address.toLowerCase() || '',
            key: this.dnStatus() && Hex.stringify(Base64.parse(this.dnStatus().validator_info.pub_key.value)).toLowerCase() || '',
        }))
        this.dn = computedFrom(this.dnStatus, this.dnPeers, (x, y) => this.summarize(x, y))
        this.bvn = computedFrom(this.bvnStatus, this.bvnPeers, (x, y) => this.summarize(x, y))

        this.operator = ko.computed(() => {
            if (!this.dnStatus())
                return null
            const { id } = this.validator()
            for (const val of this.vm.validators())
                if (val.publicKeyHash.substr(0, 40) == id)
                    return val.operator?.substr(6) || null
            return null
        })
    }

    summarize(status, peers) {
        const s = {
            id: status?.node_info.id.toLowerCase() || '',
            height: Number(status?.sync_info.latest_block_height) || null,
            peers: peers || [],
            voting_power: status?.validator_info.voting_power || '',
            listen_addr: status?.node_info.listen_addr
        }

        if (!status || !peers) {
            s.status = 'unknown'
            return s
        }

        if (/^(\w+:\/\/)?0\.0\.0\.0(:\d+)?$/.test(s.listen_addr)) {
            s.status = 'bad-listen'
            return s
        }

        const maxHeight = this.vm.maxHeight()[parsePartition(status.node_info.network)]
        if (maxHeight - s.height < 10)
            s.status = 'ready'
        else if (maxHeight - s.height < 1000)
            s.status = 'behind'
        else
            s.status = 'syncing'
        return s
    }

    matchesUrl(url) {
        return this.addressUrl().hostname == url.hostname ||
            this.dn().id == url.username ||
            this.bvn().id == url.username
    }

    async fetch(directory, service, method, params = {}) {
        const url = this.addressUrl()
        const resp = await fetch('/proxy', {
            method: 'post',
            body: JSON.stringify({
                jsonrpc: '2.0',
                id: 1,
                method,
                params: {
                    host: url.hostname,
                    basePort: Number(url.port),
                    directory,
                    service,
                    params,
                }
            })
        })
        if (!resp.ok) {
            console.warn(`Request failed with ${resp.status} ${resp.statusText}`)
            return null
        }
        const { result } = await resp.json()
        return result
    }

    async refresh() {
        await peerSem.acquire()
        try {
            await this.fetch(true,  svcAccumulateApi, 'version' ).then(x => this.accVersion(x?.data)).catch(log)
            await this.fetch(true,  svcTendermintRpc, 'net_info').then(x => this.dnPeers(x?.peers)).catch(log)
            await this.fetch(false, svcTendermintRpc, 'net_info').then(x => this.bvnPeers(x?.peers)).catch(log)
            await this.fetch(true,  svcTendermintRpc, 'status'  ).then(x => this.dnStatus(x)).catch(log)
            await this.fetch(false, svcTendermintRpc, 'status'  ).then(x => this.bvnStatus(x)).catch(log)
        } finally {
            peerSem.release()
        }
    }

    async refreshStatus() {
        // await peerSem.acquire()
        try {
            await this.fetch(true,  svcTendermintRpc, 'status'  ).then(x => this.dnStatus(x)).catch(log)
            await this.fetch(false, svcTendermintRpc, 'status'  ).then(x => this.bvnStatus(x)).catch(log)
        } finally {
            // peerSem.release()
        }
    }

    async refreshInfo() {
        // await peerSem.acquire()
        try {
            await this.fetch(true,  svcAccumulateApi, 'version' ).then(x => this.accVersion(x?.data)).catch(log)
            await this.fetch(true,  svcTendermintRpc, 'net_info').then(x => this.dnPeers(x?.peers)).catch(log)
            await this.fetch(false, svcTendermintRpc, 'net_info').then(x => this.bvnPeers(x?.peers)).catch(log)
        } finally {
            // peerSem.release()
        }
    }

    copyValidatorKey() {
        navigator.clipboard.writeText(this.validator().key).catch(log)
    }
}

function log(err) {
    console.warn(err)
}

class Semaphore {
    waiting = []

    constructor(count) {
        this.count = count
    }

    acquire() {
        if (this.count > 0)
            this.count--
        else
            return new Promise(r => this.waiting.push(r))
    }

    release() {
        if (!this.waiting.length)
            this.count++
        else
            this.waiting.shift()()
    }
}

const peerSem = new Semaphore(3)

function computedFrom(...props) {
    const fn = props.pop()
    const value = ko.observable(fn(...props.map(x => x())))
    for (const prop of props) {
        prop.subscribe(() => {
            value(fn(...props.map(x => x())))
        })
    }
    return value
}