package dashboard

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"strings"
	"sync"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	tmjson "github.com/tendermint/tendermint/libs/json"
	coretypes "github.com/tendermint/tendermint/rpc/core/types"
	"gitlab.com/accumulatenetwork/accumulate/config"
)

type WrappedRpcRequest struct {
	Host      string          `json:"host"`
	BasePort  int             `json:"basePort"`
	Directory bool            `json:"directory"`
	Service   int             `json:"service"`
	Params    json.RawMessage `json:"params"`
}

type JsonRpcProxy struct {
	mu    sync.RWMutex
	hosts map[string]bool
}

func (j *JsonRpcProxy) addAllowed(b []byte) {
	var raw json.RawMessage
	var res jsonrpc2.Response
	res.Result = &raw
	err := tmjson.Unmarshal(b, &res)
	if err != nil {
		log.Printf("Failed to unmarshal net info: %v", err)
		return
	}

	var netInfo coretypes.ResultNetInfo
	err = tmjson.Unmarshal(raw, &netInfo)
	if err != nil {
		log.Printf("Failed to unmarshal net info: %v", err)
		return
	}

	j.mu.Lock()
	defer j.mu.Unlock()

	for _, peer := range netInfo.Peers {
		if j.hosts[peer.RemoteIP] {
			continue
		}

		fmt.Printf("Allowing %s\n", peer.RemoteIP)
		j.hosts[peer.RemoteIP] = true
	}
}

func (j *JsonRpcProxy) isAllowed(host string) bool {
	ip := net.ParseIP(host) != nil

	j.mu.RLock()
	defer j.mu.RUnlock()

	if ip {
		return j.hosts[host]
	}

	for {
		if j.hosts[host] {
			return true
		}

		parts := strings.SplitN(host, ".", 2)
		if len(parts) == 1 {
			return false
		}
		host = parts[1]
	}
}

func (j *JsonRpcProxy) writeError(req jsonrpc2.Request, w http.ResponseWriter, statusCode int, errCode jsonrpc2.ErrorCode, msg string, data any) {
	w.WriteHeader(statusCode)
	res := new(jsonrpc2.Response)
	res.ID = req.ID
	res.Error = jsonrpc2.NewError(errCode, msg, data)
	_ = json.NewEncoder(w).Encode(res)
}

func (j *JsonRpcProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	w.Header().Add("Content-Type", "application/json")

	b, err := io.ReadAll(r.Body)
	if err != nil {
		log.Printf("Failed to read body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	r.Body.Close()

	var req jsonrpc2.Request
	err = json.Unmarshal(b, &req)
	if err != nil {
		j.writeError(req, w, http.StatusBadRequest, jsonrpc2.ErrorCodeInvalidParams, "failed to parse request", err)
		return
	}

	var wrapped WrappedRpcRequest
	err = json.Unmarshal(req.Params.(json.RawMessage), &wrapped)
	if err != nil {
		j.writeError(req, w, http.StatusBadRequest, jsonrpc2.ErrorCodeInvalidParams, "failed to parse params", err)
		return
	}

	if !j.isAllowed(wrapped.Host) {
		j.writeError(req, w, http.StatusBadRequest, jsonrpc2.ErrorCodeInvalidParams, "bad host", "host is not in the whitelist")
		return
	}

	port := wrapped.BasePort + wrapped.Service
	if wrapped.Directory {
		port += config.PortOffsetDirectory
	} else {
		port += config.PortOffsetBlockValidator
	}

	host := fmt.Sprintf("http://%s:%d", wrapped.Host, port)
	if wrapped.Service == int(config.PortOffsetAccumulateApi) {
		host += "/v2"
	}

	req.Params = wrapped.Params
	b, err = json.Marshal(req)
	if err != nil {
		log.Printf("Failed to marshal request: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	res, err := http.Post(host, "application/json", bytes.NewReader(b))
	if err != nil {
		j.writeError(req, w, http.StatusGone, jsonrpc2.ErrorCodeInternal, "proxy request failed", err)
		return
	}

	b, err = io.ReadAll(res.Body)
	if err != nil {
		log.Printf("Failed to read response body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	res.Body.Close()

	if wrapped.Directory && wrapped.Service == int(config.PortOffsetTendermintRpc) && req.Method == "net_info" {
		j.addAllowed(b)
	}

	w.WriteHeader(res.StatusCode)
	_, err = w.Write(b)
	if err != nil {
		log.Printf("Failed to proxy response: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
