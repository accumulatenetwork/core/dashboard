FROM golang:1.18 as build

# Build
WORKDIR /root
COPY . .
ENV CGO_ENABLED 0
RUN go build ./cmd/dashboard

FROM alpine:3

# Copy binaries
COPY --from=build /root/dashboard /bin/

ENTRYPOINT ["dashboard"]
CMD []